import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Items from "./components/Items";

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            orders: [],
            items: [
                {
                    id: 1,
                    title: 'Chair',
                    img: 'chair1.jpg',
                    desc: 'Lorem ipsum dolor sit amet',
                    category: 'chairs',
                    price: '19.99'
                },

                {
                    id: 2,
                    title: 'Orange Sofa',
                    img: 'orange-sofa.jpg',
                    desc: 'Lorem ipsum dolor sit amet',
                    category: 'sofa',
                    price: '199.99'
                },

                {
                    id: 3,
                    title: 'White Sofa',
                    img: 'sofa1.webp',
                    desc: 'Lorem ipsum dolor sit amet',
                    category: 'sofa',
                    price: '99.99'
                },

                {
                    id: 4,
                    title: 'Table',
                    img: 'table.webp',
                    desc: 'Lorem ipsum dolor sit amet',
                    category: 'Table',
                    price: '60.50'
                },

                {
                    id: 5,
                    title: 'Wooden Chair',
                    img: 'wood-chair.webp',
                    desc: 'Lorem ipsum dolor sit amet',
                    category: 'chairs',
                    price: '19.99'
                },
            ]
        }
        this.addToOrder = this.addToOrder.bind(this)
    }
    render() {
        return (
            <div className="wrapper">
                <Header orders={this.state.orders} />
                <Items items={this.state.items} onAdd={this.addToOrder} />
                <Footer />
            </div>
        )
    }

    addToOrder(item) {
        let isInArray = false
        this.state.orders.forEach(el => {
            if(el.id === item.id) {
                isInArray = true
            }
        })
        if (!isInArray){
            this.setState({orders: [...this.state.orders, item]})
        }
    }
}

export default App;